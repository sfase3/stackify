import "./HomeMainbar.css";
import { Link, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import QuestionList from "./QuestionList";
import { useContext, useMemo } from "react";
import { SearchContext } from "../../context/SearchContext";

function HomeMainbar() {
  const questionsList = useSelector((state) => state.QuestionReducer);
  const {searchValue} = useContext(SearchContext);
  const location = useLocation();

  // const filterData = useMemo(() => {
  //   return questionsList.data?.filter(el => el.questionTitle.includes(searchValue))
  // }, [searchValue]);

  return (
    <div className="main-bar">
      <div className="main-bar-header">
        {location.pathname === "/" ? (
          <h1>Top Questions</h1>
        ) : (
          <h1>All Questions</h1>
        )}
        <Link to="/AskQuestion" className="ask-btn">
          Ask a Question
        </Link>
      </div>
      <div>
        {questionsList?.data === null ? (
          <>
            <h1>Loading...</h1>
          </>
        ) : (
          <>
            <p>{questionsList?.data?.length} questions</p>
            <QuestionList questionsList={questionsList?.data} />
          </>
        )}
      </div>
    </div>
  );
}

export default HomeMainbar;
