import { render, screen } from '@testing-library/react';
import Tags from '../pages/tags/Tags';
// import LeftSidebar from '../components/leftsidebar/LeftSidebar';
import TagsList from '../pages/tags/TagsList';
import '@testing-library/jest-dom';

// Mocking the child components
jest.mock("../components/leftsidebar/LeftSidebar", () => {
  return jest.fn(() => <div>LeftSidebarMock</div>);
});

jest.mock("../pages/tags/TagsList", () => {
  return jest.fn(({ tag }) => <div>{tag.tagName}</div>);
});

describe('Tags Component', () => {
  // Test if the component renders without crashing
  it('renders without crashing', () => {
    render(<Tags />);
    expect(screen.getByText('Tags')).toBeInTheDocument();
    expect(screen.getByText('A tag is a keyword or label that categorizes your question with other, similar questions.')).toBeInTheDocument();
  });

  // Test if all tags are rendered
  it('renders all tags correctly', () => {
    render(<Tags />);
    expect(screen.getByText('javascript')).toBeInTheDocument();
    expect(screen.getByText('python')).toBeInTheDocument();
    expect(screen.getByText('java')).toBeInTheDocument();
    expect(screen.getByText('cs#')).toBeInTheDocument();
    expect(screen.getByText('ruby')).toBeInTheDocument();
    expect(screen.getByText('swift')).toBeInTheDocument();
    expect(screen.getByText('php')).toBeInTheDocument();
    expect(screen.getByText('typescript')).toBeInTheDocument();
    expect(screen.getByText('go')).toBeInTheDocument();
    expect(screen.getByText('rust')).toBeInTheDocument();
  });

  // Test if LeftSidebar and TagsList components are rendered
  it('renders LeftSidebar and TagsList components', () => {
    render(<Tags />);
    expect(screen.getByText('LeftSidebarMock')).toBeInTheDocument();
    // Assuming there's a unique part of the TagsList that can be checked
    expect(TagsList).toHaveBeenCalledTimes(10); // There are 10 tags
  });
});