import "./LeftSidebar.css";
import { NavLink } from "react-router-dom";

function LeftSidebar() {
  return (
    <div className="left-sidebar">
      <div className="side-nav">
        <NavLink to="/" className="side-nav-links" activeclass="active">
          <p>Home</p>
        </NavLink>
        <div className="side-nav-div">
          <div>
            <p>PUBLIC</p>
            <NavLink
              to="/Questions"
              className="side-nav-links"
              activeclass="active"
            >
              <p>Questions</p>
            </NavLink>
            <NavLink
              to="/Tags"
              className="side-nav-links"
              activeclass="active"
            >
              <p>Tags</p>
            </NavLink>
            <NavLink
              to="/Users"
              className="side-nav-links"
              activeclass="active"
            >
              <p>Users</p>
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LeftSidebar;
